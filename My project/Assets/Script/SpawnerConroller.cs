using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerConroller : MonoBehaviour
{
    [SerializeField]
    private GameObject m_Capsula;
    [SerializeField]
    private GameObject m_HealthCapsulas;
    [SerializeField]
    private GameObject m_EvilCapsulas;
    [SerializeField]
    private float m_SpawnRate;
    //private float m_SpawnRateDelta = 3f;

    [SerializeField]
    private GameObject[] m_SpawnPoints;

    GameObject spawned;
    CapsuleController bala;
    private float VelMax;
    private float VelMin;


    // Start is called before the first frame update
    void Start()
    {
        m_SpawnRate = 3f;
        VelMax = 10f;
        VelMin = 5f;

        StartCoroutine(SpawnCoroutine());
        StartCoroutine(IncreaseVelocity());
        StartCoroutine(IncreaseSpawnRate());
        StartCoroutine(SpanwHealthCapsule());
        StartCoroutine(SpanwEvilCapsule());
    }

    IEnumerator SpanwEvilCapsule()
    {
        while (true)
        {
            yield return new WaitForSeconds(5f); 
            Spawnear(m_EvilCapsulas);
        }
    }
    IEnumerator SpanwHealthCapsule()
    {
        while (true)
        {
            yield return new WaitForSeconds(6f); 
            Spawnear(m_HealthCapsulas);
        }
    }

    IEnumerator SpawnCoroutine()
    {
        while (true)
        {
            Spawnear(m_Capsula);
            yield return new WaitForSeconds(m_SpawnRate);
        }
    }

    IEnumerator IncreaseSpawnRate()
    {
        while (true)
        {
            yield return new WaitForSeconds(3);
            m_SpawnRate -= 0.5f;

            if (m_SpawnRate <= 0.5f)
                m_SpawnRate = 0.5f;
        }

    }

    IEnumerator IncreaseVelocity()
    {
        while (true)
        {
            bala.VeloMax = VelMax;
            bala.VeloMin = VelMin;

            yield return new WaitForSeconds(5);

            VelMax += 5f;
            VelMin += 5f;

            if (VelMax >= 30f)
            {
                VelMax = 30f;
            }
            if (VelMin >= 20f)
            {
                VelMin = 20f;
            }

        }
    }

    void Spawnear(GameObject spawned)
    {
        spawned = Instantiate(spawned);
        spawned.transform.position = new Vector3(
            m_SpawnPoints[Random.Range(0, m_SpawnPoints.Length)].gameObject.transform.position.x,
            Random.Range(10.0f, -9.0f),
            0);

        bala = spawned.GetComponent<CapsuleController>();
        if (spawned.transform.position.x < 0)
        {
            bala.Sentido = 1;
        }
        else
        {
            bala.Sentido = -1;
        }

        bala.VeloMax = VelMax;
        bala.VeloMin = VelMin;
    }

}

