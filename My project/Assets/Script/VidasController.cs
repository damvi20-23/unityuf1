using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VidasController : MonoBehaviour
{
    GameObject[] Vidas;

    [SerializeField]
    MyCircleController player;

    // Start is called before the first frame update
    void Awake()
    {
        Vidas = new GameObject[transform.childCount];

        for(int i = 0; i < Vidas.Length; ++i)
            Vidas[i] = transform.GetChild(i).gameObject;

        player.OnLifeChanged += ActualitzarVida;
    }

    void ActualitzarVida(int hp)
    {
        for (int i = 0; i < hp; ++i)
            Vidas[i].SetActive(true);

        for (int i = hp; i < Vidas.Length; ++i)
            Vidas[i].SetActive(false);
    }
}
