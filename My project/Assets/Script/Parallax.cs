using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Parallax : MonoBehaviour
{
    public RawImage a;
    public float x, y;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        a.uvRect = new Rect(a.uvRect.position + new Vector2(x, y) * Time.deltaTime, a.uvRect.size);
    }
}
