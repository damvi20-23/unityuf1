using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIPuntuacioGO : MonoBehaviour
{
    private GameManager m_gm;
    private TMPro.TextMeshProUGUI m_Text;

    private void Start()
    {
        m_Text = GetComponent<TMPro.TextMeshProUGUI>();
        m_gm = GameManager.Instance;
        m_Text.text = m_gm.Score.ToString();
    }
}
