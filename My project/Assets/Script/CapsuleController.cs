using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleController : MonoBehaviour
{

    //[SerializeField]
    private float m_veloProjectil;

    //private float[] m_rangVelo = new float[2];

    private float m_veloMax;
    public float VeloMax
    {
        get { return m_veloMax; }
        set { m_veloMax = value; }
    }

    private float m_veloMin;
    public float VeloMin
    {
        get { return m_veloMin; }
        set { m_veloMin = value; }
    }

    private int m_sentido;
    public int Sentido
    {
        get { return m_sentido; }
        set { m_sentido = value; }
    }


    private Rigidbody2D m_rigidBody;

    void Awake()
    {
        m_rigidBody = GetComponent<Rigidbody2D>();
    }

    // Start is called before the first frame update
    void Start()
    {
        m_veloProjectil = Random.Range(m_veloMax, m_veloMin);
        //Debug.Log("Velocitat Projectil -> "+m_veloProjectil);
        m_rigidBody.velocity = new Vector3((m_veloProjectil*m_sentido), (m_rigidBody.velocity.y) / 4, 0);
    }

}
