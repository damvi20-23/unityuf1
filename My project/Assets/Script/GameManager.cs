using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    static private GameManager m_gm;
    public static GameManager Instance { get { return m_gm; } }

    public MyCircleController m_player;

    private float m_Score;  // camp privat que utilitzem
    public float Score    // Accessor (getter) del camp anterior
    {
        get => m_Score;
        set => m_Score = value;
    }

    private SpawnerConroller m_sp;
    
    private void Awake()
    {
        if (m_gm == null)
            m_gm = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
        SceneManager.sceneLoaded += OnSceneLoaded;
        InitValues();

        StartCoroutine(CountScore());
    }

    IEnumerator CountScore()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            m_Score += 1;
        }
    }

    //Es crida al carregar una escena
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("OnSceneLoaded: " + scene.name);
        if (scene.name == "GameScene")
            InitValues();
    }

    private void InitValues()
    {
        Score = 0;

    }

    public void GameOver()
    {
        SceneManager.LoadScene("GameOver");
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P) 
            && SceneManager.GetActiveScene() == SceneManager.GetSceneByName("GameOver") 
            || SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Intro"))
        {
            SceneManager.LoadScene("GameScene");
        }

    }

}
