using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class MyCircleController : MonoBehaviour
{
    [SerializeField]
    private float m_velocitat;
    [SerializeField]
    private float m_JumpSpeed;

    private bool m_onAir = true;

    private Rigidbody2D m_rigidBody;

    public int m_vida = 3;

    public delegate void LifeChanged(int life);
    public event LifeChanged OnLifeChanged;

    void Awake()
    {
        m_rigidBody = GetComponent<Rigidbody2D>();
    }

    void Start()
    {
        OnLifeChanged.Invoke(m_vida);
        //DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.D))
        {
            m_rigidBody.velocity = new Vector3(m_velocitat, m_rigidBody.velocity.y, 0);
        }
        if (Input.GetKey(KeyCode.A))
        {
            m_rigidBody.velocity = new Vector3(-m_velocitat, m_rigidBody.velocity.y, 0);
        }
        if ((Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.Space)) && !m_onAir)
        {
            m_rigidBody.velocity = new Vector3(m_rigidBody.velocity.x, m_JumpSpeed, 0);
        }
        if(Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D))
        {
            m_rigidBody.velocity = new Vector3(0, m_rigidBody.velocity.y, 0);
        }
    }



    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Terra")
        {
            //Debug.Log("Player -> Hit Terra");
            m_onAir = false;
        }
    }

    void OnCollisionExit2D(Collision2D col)
    {
        if (col.gameObject.tag == "Terra")
        {
            m_onAir = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Projectil")
        {
            Debug.Log("Cirlce -> "+ col.gameObject.name);
            if (col.gameObject.name == "Capsule(Clone)")
            {
                ReceiveHealth(-1);
            }
            if (col.gameObject.name == "HealthCapsule(Clone)")
            {
                if(m_vida<3)
                ReceiveHealth(1);

            }
            if (col.gameObject.name == "EvilCapsule(Clone)")
            {
                ReceiveHealth(-2);
            }

            Destroy(col.gameObject);
        }
    }

    private void ReceiveHealth(int health)
    {
        m_vida += health;
        if (m_vida < 0)
            m_vida = 0;

        Debug.Log("Cirlce -> " + m_vida);
        OnLifeChanged.Invoke(m_vida);

        if (m_vida == 0)
            GameManager.Instance.GameOver();
    }



}
