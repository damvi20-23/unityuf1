using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIScore : MonoBehaviour
{
    private GameManager m_gm;
    private TMPro.TextMeshProUGUI m_Text;

    void Start()
    {
        m_Text = GetComponent<TMPro.TextMeshProUGUI>();
        m_gm = GameManager.Instance;
    }

    // Update is called once per frame
    void Update()
    {
        m_Text.text=m_gm.Score.ToString();
    }
}
